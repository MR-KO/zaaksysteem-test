# Base image to start from
FROM python:3

# Set directory for our app and copy all files to it
WORKDIR /usr/src/test_app
COPY . .

# Install dependencies
RUN pip install --no-cache-dir -r requirements.txt

# Expose debug port 5000
EXPOSE 5000

# Run our flask app
CMD ["python3", "./test.py"]