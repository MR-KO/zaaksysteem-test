import os

from flask import request

# FlaskAPI for REST API stuff: https://www.flaskapi.org/
from flask_api import FlaskAPI, status, exceptions

# https://github.com/danthedeckie/simpleeval
from simpleeval import simple_eval

app = FlaskAPI(__name__)


# API documentation: https://zaaksysteemtest.docs.apiary.io/#
API_DOC_URL = "https://zaaksysteemtest.docs.apiary.io/#"



# Basic routes defined
ROUTE_INDEX = "/"
ROUTE_CALC = "/calc/"
ROUTE_CALC_GET = ROUTE_CALC + "<path:calc_str>/"

# Key to use to extract expression string
API_CALCULATE_KEY = "calculate"

API_ERROR_FIELD = "error"
API_RESULT_FIELD = "result"

# Success and error messages used by the API
RESULT_OF_CALC_TEXT = "Result of '{}'"
ERROR_INVALID_INCOMPLETE_TEXT = "Error, invalid or incomplete request data!"
ERROR_GENERAL_TEXT = "Error!"


def api_response(text_str="Hello world", **kwargs):
	"""
	Helper function to return an API response for FlaskAPI.

	Ensures the basic (default) template of FlaskAPI is called and rendered,
	allowing for browser-based evaluation of our API (like Django-REST).

	TODO: Ditch default template and make our own.
	"""
	return_dict = {
		"url": request.base_url,
		"text": text_str,
	}

	return_dict.update(**kwargs)
	return return_dict


def _helper_calc(calc_str):
	""" Helper function to calculate expression result and return API response. """
	try:
		# Use the default simple_eval function, no complex stuff is supported yet
		result = simple_eval(calc_str)
		return api_response(RESULT_OF_CALC_TEXT.format(calc_str),
			**{ API_RESULT_FIELD: result }), 200

	except Exception as e:
		return api_response(ERROR_GENERAL_TEXT, **{ API_ERROR_FIELD: str(e) }), 400



@app.route(ROUTE_INDEX, methods=["GET"])
def index():
	return api_response("API documentation: \"{}\"".format(API_DOC_URL))


@app.route(ROUTE_CALC, methods=["GET", "POST"])
def calc():
	if request.method == "GET":
		return api_response("POST to {} to get your ".format(ROUTE_CALC) +
			"calculated results back, or use the GET version at /calc/<string:calc_string>. Check out " +
			"\"{}\" for usage info.".format(API_DOC_URL))

	if request.method == "POST":
		# Parse request data
		if API_CALCULATE_KEY not in request.data:
			return api_response(ERROR_GENERAL_TEXT,
				**{ API_ERROR_FIELD: ERROR_INVALID_INCOMPLETE_TEXT }), 400

		# Returns result of calculation in calc_str using simple_eval
		calc_str = request.data[API_CALCULATE_KEY]
		return _helper_calc(calc_str)


@app.route(ROUTE_CALC_GET, methods=["GET"])
def calc_get(calc_str):
	return _helper_calc(calc_str)



if __name__ == "__main__":
	app.run(debug=True, host="0.0.0.0", port=int(os.environ.get("PORT", 5000)))